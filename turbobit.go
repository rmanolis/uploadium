package main

import (
	"errors"
	"log"
	"net/http"

	"net/url"
	"strings"

	"golang.org/x/net/html"
)

type turbobit struct{}

func (tu *turbobit) Domain() string {
	return "turbobit.net"
}

func (tu *turbobit) Link() string {
	return "https://turbobit.net"
}

func (tu *turbobit) Login(username, password string) (int, error) {
	return http.StatusUnauthorized, errors.New("Not implemented")
}

func (tu *turbobit) addHeaders(req *http.Request) {
	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0")
	req.Header.Add("Accept-Language", "en-US,en;q=0.5")
	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	req.Header.Add("DNT", "1")
	req.Header.Add("Host", "turbobit.net")
	req.Header.Add("TE", "Trailers")
	bigCookie := ""
	for _, v := range cookies {
		c := v
		req.AddCookie(&c)
		bigCookie += c.Name + "=" + c.Value + "; "
	}
	req.Header.Add("Cookie", bigCookie)
}
func (tu *turbobit) GetDownloadUrl(path string) (*url.URL, int, error) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}
	tu.addHeaders(req)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}
	z := html.NewTokenizer(resp.Body)
	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			return nil, resp.StatusCode, errors.New("Document is done, url not found")
		case tt == html.StartTagToken:
			t := z.Token()
			if t.Data == "a" {
				for _, v := range t.Attr {
					if v.Key == "href" {
						if strings.Contains(v.Val, "/download/redirect") {
							log.Println("Found link ", v.Val)
							req, err := http.NewRequest("GET", v.Val, nil)
							if err != nil {
								log.Println("Error: ", err)
								return nil, 0, err
							}
							tu.addHeaders(req)
							resp, err := client.Do(req)
							if err != nil {
								log.Println("Error: ", err)
								return nil, 0, err
							}
							return resp.Request.URL, resp.StatusCode, nil
						}
					}
				}
			}
		}
	}
}
