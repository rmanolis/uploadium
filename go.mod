module bitbucket.org/rmanolis/uploadium

require (
	github.com/frankban/quicktest v1.1.0 // indirect
	github.com/juju/go4 v0.0.0-20160222163258-40d72ab9641a // indirect
	github.com/juju/persistent-cookiejar v0.0.0-20171026135701-d5e5a8405ef9
	golang.org/x/crypto v0.0.0-20181127143415-eb0de9b17e85
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc
	golang.org/x/sys v0.0.0-20181128092732-4ed8d59d0b35 // indirect
	gopkg.in/errgo.v1 v1.0.0 // indirect
	gopkg.in/retry.v1 v1.0.2 // indirect
)
