package main

import (
	"errors"
	"log"
	"net/http"

	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)


type uploadTo struct{}

func (u *uploadTo) Domain() string {
	return "uploaded.net"
}

func (u *uploadTo) Login(username, password string) (int, error) {

	form := url.Values{}
	form.Add("id", username)
	form.Add("pw", password)
	req, err := http.NewRequest("POST", "http://uploaded.net/io/login", strings.NewReader(form.Encode()))
	if err != nil {
		log.Println("Error: ", err)
		return 0, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(form.Encode())))

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return 0, err
	}

	return resp.StatusCode, nil
}

func (u *uploadTo) GetDownloadUrl(path string) (*url.URL, int, error) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}

	z := html.NewTokenizer(resp.Body)

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return nil, resp.StatusCode, errors.New("Document is done, url not found")
		case tt == html.StartTagToken:
			t := z.Token()

			isForm := t.Data == "form"
			if isForm {
				for _, a := range t.Attr {
					if a.Key == "action" {
						log.Println("Found link:", a.Val)
						url, err := url.Parse(a.Val)
						if err != nil {
							log.Println("Error: ", err)
						}
						return url, resp.StatusCode, nil
					}
				}
			}
		}
	}
}
