package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"os"
	"strings"
)

var client = new(http.Client)
var cookies = []http.Cookie{}

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

type Website interface {
	GetDownloadUrl(path string) (*url.URL, int, error)
	Login(username, password string) (int, error)
	Domain() string
}

var webs = map[string]Website{}

func init() {
	webs["uploaded"] = &uploadTo{}
	webs["fboom"] = &fboomMe{}
	webs["turbobit"] = &turbobit{}
}

func getWebsite(link string) (Website, error) {
	for _, v := range webs {
		if strings.Contains(link, v.Domain()) {
			return v, nil
		}
	}
	return nil, errors.New("Website is not found for link: " + link)
}

func submitCookitToTheClient(domain, cookieFile string) error {
	log.Println("Submit the cookie to the client")
	b, err := ioutil.ReadFile(cookieFile)
	if err != nil {
		return err
	}
	json.Unmarshal(b, &cookies)
	/*
		cookieJar, err := cookiejar.New(nil)
		if err != nil {
			return err
		}
		if len(cookies) == 0 {
			return errors.New("The cookies are empty")
		}
		web := webs[domain]
		u, err := url.Parse(web.Domain())
		if err != nil {
			return err
		}
		newCookies := []*http.Cookie{}
		for _, v := range cookies {
			c := v
			newCookies = append(newCookies, &c)
		}

		cookieJar.SetCookies(u, newCookies)
		log.Fatal(cookieJar.Cookies(u))
		client = &http.Client{
			Jar: cookieJar,
		}*/
	return nil
}

func run(links []string, kbps int, directory string) {

	for _, link := range links {
		log.Println("Start downloading ", link)
		web, err := getWebsite(link)
		if err != nil {
			log.Println("Error: Could not find domain for link " + link)
			continue
		}
		log.Println("From: ", web.Domain())
		url, _, err := web.GetDownloadUrl(link)
		if err != nil || url == nil {
			log.Println("Error:", err)
			continue
		}
		log.Println("The link for download is ", url)
		download(kbps, directory, url)
		log.Println("The download finish for ", link)
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	kbps := flag.Int("k", 0, "Limit the speed in kb/s")
	directory := flag.String("d", "", "Folder to put the files")
	listFile := flag.String("l", "", "A text with a list of ul.to urls to download")
	cookieFile := flag.String("c", "", "The file with the cookie in json format")
	domain := flag.String("m", "", "The domain: fboom, uplodium or turbobit")
	flag.Parse()

	if len(*directory) == 0 {
		fmt.Println("Error: add the directory to download the files with -d ")
		return
	}
	if len(*domain) == 0 {
		fmt.Println("Error: add the domain")
		return
	}
	_, ok := webs[*domain]
	if !ok {
		fmt.Println("Error: the domain ", domain, " does not exists.")
		return
	}
	if len(*cookieFile) == 0 {
		fmt.Println("Error: add the cookie in json format that is in a file with -c ")
		return
	}
	exists, _ := pathExists(*directory)
	if !exists {
		fmt.Println("Error: the directory does not exists ", *listFile)
		return
	}
	if len(*listFile) == 0 {
		fmt.Println("Error: add the text with the list of urls with -l ")
		return
	}
	exists, _ = pathExists(*listFile)
	if !exists {
		fmt.Println("Error: the list does not exists here ", *listFile)
		return
	}

	b, err := ioutil.ReadFile(*listFile)
	if err != nil {
		log.Println("Error: ", err)
		return
	}
	urls := strings.Split(string(b), "\n")
	err = submitCookitToTheClient(*domain, *cookieFile)
	if err != nil {
		log.Println("Error: ", err)
		return
	}
	run(urls, *kbps, *directory)
}
