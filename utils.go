package main

import (
	"log"
	"net/url"
	"os"
	"os/exec"
	"strconv"
)

func download(kbps int, folder string, url *url.URL) error {
	attrs := []string{"-P", folder, "--progress=bar:force", "--continue", "--retry-connrefused",
		"--waitretry=1", "--read-timeout=20", "--timeout=15", "--tries=0", "--content-disposition"}
	if kbps != 0 {
		attrs = append(attrs, "--limit-rate="+strconv.Itoa(kbps)+"k")
	}
	name := url.Query().Get("name")
	if len(name) > 0 {
		attrs = append(attrs, "--output-document="+folder+"/"+name)
	}
	attrs = append(attrs, url.String())
	cmd := exec.Command("wget", attrs...)
	log.Println(cmd.Args)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}
