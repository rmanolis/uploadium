package main

import (
	"errors"
	"log"
	"net/http"

	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

type fboomMe struct{}

func (f *fboomMe) Domain() string {
	return "fboom.me"
}

func (f *fboomMe) Link() string {
	return "https://fboom.me"
}

func (f *fboomMe) Login(username, password string) (int, error) {

	form := url.Values{}
	form.Add("LoginForm[username]", username)
	form.Add("LoginForm[password]", password)
	req, err := http.NewRequest("POST", "https://fboom.me/login.html", strings.NewReader(form.Encode()))
	if err != nil {
		log.Println("Error: ", err)
		return 0, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(form.Encode())))

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return 0, err
	}
	return resp.StatusCode, nil
}

func (f *fboomMe) GetDownloadUrl(path string) (*url.URL, int, error) {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return nil, 0, err
	}

	z := html.NewTokenizer(resp.Body)

	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return nil, resp.StatusCode, errors.New("Document is done, url not found")
		case tt == html.StartTagToken:
			t := z.Token()
			if t.Data == "a" {
				for _, v := range t.Attr {
					if v.Key == "href" {
						if strings.Contains(v.Val, "/file/url") {
							req, err := http.NewRequest("GET", f.Link()+v.Val, nil)
							if err != nil {
								log.Println("Error: ", err)
								return nil, 0, err
							}
							resp, err := client.Do(req)
							if err != nil {
								log.Println("Error: ", err)
								return nil, 0, err
							}
							return resp.Request.URL, resp.StatusCode, nil
						}
					}
				}
			}
		}
	}
}
